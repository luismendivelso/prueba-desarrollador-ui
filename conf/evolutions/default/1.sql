# --- !Ups
CREATE TABLE user (
  id                        INT NOT NULL AUTO_INCREMENT ,
  username                  VARCHAR(50) NOT NULL ,
  email                     VARCHAR(50) NOT NULL ,
  password                  VARCHAR(100) NOT NULL ,
  name                      VARCHAR(50) NOT NULL ,
  PRIMARY KEY (id),
  UNIQUE usuario_username (username),
  UNIQUE usuario_correo (email))
;

CREATE TABLE song (
  id                        INT NOT NULL AUTO_INCREMENT ,
  user_id                   INT NOT NULL  ,
  name                      VARCHAR(50) NOT NULL ,
  path                      VARCHAR(100) NOT NULL ,
  genre                     VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`id`))
;


# --- !Downs

DROP TABLE user;
DROP TABLE song;