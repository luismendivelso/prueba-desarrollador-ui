package models;

import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Esta clase define el modelo User en la base de datos con la informacion correspondiente para el inicio de sesion
 *
 * Created by Makabeo on 22/05/2017.
 */
@Entity
public class User
{
    @Id
    @GeneratedValue
    private int id;

    private String name;

    private String username;

    private String password;

    private String email;

    public User(String name, String username, String password, String email)
    {
        this.name = name;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public User()
    {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Método que guarda un usuario en la base de datos
     *
     */
    public void save()
    {
        JPA.em().persist(this);
    }

    /**
     * Método que devuelve un usuario dada su id
     * @return un usuario conservada en la base datos
     */
    public User find(int id)
    {
        return JPA.em().find(User.class,id);
    }
}
