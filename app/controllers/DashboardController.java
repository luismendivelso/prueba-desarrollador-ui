package controllers;

import models.Song;
import models.User;
import org.h2.store.fs.FileUtils;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.twirl.api.Html;
import views.html.dashboardSite.*;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * Created by Makabeo on 21/05/2017.
 */
public class DashboardController extends Controller
{
    @Transactional(readOnly=true)
    public static Result index() {
        if (session("user") != null)
        {
            List<User> usuarios = JPA.em().createQuery(
                    "SELECT DISTINCT u FROM User u WHERE username LIKE :custUsername")
                    .setParameter("custUsername", session("user"))
                    .getResultList();
            List<Song> canciones = JPA.em().createQuery(
                    "SELECT s FROM Song s WHERE user_id = :custUserId")
                    .setParameter("custUserId", usuarios.get(0).getId())
                    .getResultList();
            Html navbar = navbarDashboard.render();
            Html sidebar = sidebarDashboard.render();
            Html footer = dfooter.render();
            Html contenido = contentDashboard.render(usuarios.get(0),canciones);
            return ok(indexDashboard.render(navbar, sidebar, footer, contenido));
        }
        else
        {
            flash("error", "You are not logged in");
            return redirect("/register");
        }
    }

    @Transactional
    public static play.mvc.Result createSong() {

        DynamicForm form = Form.form().bindFromRequest();
        String nameSong = form.get("nameSong");
        String songGenre = form.get("genre");
        String usuarioid = form.get("usuarioid");

        play.mvc.Http.MultipartFormData body = request().body().asMultipartFormData();
        play.mvc.Http.MultipartFormData.FilePart song = body.getFile("songArchive");

        if (song != null)
        {
            String fileName = song.getFilename();
            String contentType = song.getContentType();
            java.io.File file = song.getFile();
            if (contentType.equalsIgnoreCase("audio/mp3")||contentType.equalsIgnoreCase("audio/ogg")||contentType.equalsIgnoreCase("audio/wav"))
            {
                if (!nameSong.equals(null) && !songGenre.equals(null) && !usuarioid.equals(null))
                {
                    try
                    {
                        Song cancionNueva = new Song(Integer.parseInt(usuarioid),nameSong,("songs/"+fileName),songGenre);
                        cancionNueva.save();
                        file.renameTo(new File("public/songs", fileName));
                    }
                    catch (Exception ioe)
                    {
                        System.out.println("Problem operating on filesystem");
                    }
                    flash("success", "The song was uploaded ");
                    return redirect("/dashboard");
                }
                else
                {
                    flash("error", "Fields can not be empty");
                    return redirect("/dashboard");
                }
            }
            else
            {
                flash("error", "Format is not supported");
                return redirect("/dashboard");
            }
        }
        else
        {
            flash("error", "Missing file");
            return redirect("/dashboard");
        }
    }

    @Transactional
    public static Result modifyUser()
    {
        DynamicForm form = Form.form().bindFromRequest();

        if (!form.data().isEmpty())
        {
            String email = form.get("email");
            String name = form.get("name");
            String usuarioid = form.get("usuarioid");
            if (!usuarioid.equals(null)  || !usuarioid.equals(""))
            {
                User usuario = new User();
                usuario = usuario.find(Integer.parseInt(usuarioid));
                if (!email.equals("") || !email.equals(""))
                {
                    usuario.setEmail(email);
                }
                if (!name.equals("") || !name.equals(""))
                {
                    usuario.setName(name);
                }
                usuario.save();
                flash("success", "Profile was successfully updated name = " + usuario.getName() + " email =" + usuario.getEmail());
                return redirect("/dashboard");
            }
            else
            {
                flash("error", "user not found");
                return redirect("/dashboard");
            }
        }
        else
        {
            flash("error", "Please fill in the fields");
            return redirect("/dashboard");
        }
    }

    @Transactional
    public static Result changePassword()
    {
        DynamicForm form = Form.form().bindFromRequest();

        if (!(form.data().size() == 0))
        {
            String password = form.get("modifyPassword");
            String confirm = form.get("confirmModifyPassword");
            String usuarioid = form.get("usuarioid");
            if (password.equals(confirm))
            {
                if (!usuarioid.equals(null) || !usuarioid.equals(""))
                {
                    User usuario = new User();
                    usuario = usuario.find(Integer.parseInt(usuarioid));
                    usuario.setPassword(password);
                    usuario.save();
                    flash("success", "The password was successfully updated");
                    return redirect("/dashboard");
                }
                else
                {
                    flash("error", "user not found");
                    return redirect("/dashboard");
                }
            }
            else
            {
                flash("error", "The fields are not equal");
                return redirect("/dashboard");
            }
        }
        else
        {
            flash("error", "Please fill in the fields");
            return redirect("/dashboard");
        }
    }

    @Transactional
    public static Result deleteSong()
    {
        DynamicForm form = Form.form().bindFromRequest();
        String cancionid = form.get("cancionid");
        if (!cancionid.equals(null)||!cancionid.equals(""))
        {
            Song cancion = new Song();
            cancion = cancion.find(Integer.parseInt(cancionid));
            if (!cancion.equals(null))
            {
                File file = new File("public/"+cancion.getPath());
                file.delete();
                JPA.em().createQuery(
                        "DELETE FROM Song  WHERE id = :custUserId")
                        .setParameter("custUserId", Integer.parseInt(cancionid))
                        .executeUpdate();
                flash("success", "The song was successfully deleted");
                return redirect("/dashboard");
            }
            else
            {
                flash("error", "Song not found");
                return redirect("/dashboard");
            }
        }
        else
        {
            flash("error", "Song id is not correct");
            return redirect("/dashboard");
        }
    }
}
