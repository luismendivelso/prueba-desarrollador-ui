name := "prueba-desarrollador-ui"

version := "1.0"

lazy val `prueba-desarrollador-ui` = (project in file(".")).enablePlugins(PlayJava)


scalaVersion := "2.11.7"

libraryDependencies += evolutions

libraryDependencies += filters

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.34"

libraryDependencies ++= Seq( javaJdbc ,  cache , javaWs, evolutions )

libraryDependencies ++= Seq(
  javaJpa,
  "org.hibernate" % "hibernate-entitymanager" % "4.3.9.Final" // replace by your jpa implementation
)

PlayKeys.externalizeResources := false

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"  